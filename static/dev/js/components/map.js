import '../../stylus/components/styles/custom-window.styl';

export function map() {
    
    //Подгружаем скрипт api и присоединяем на страницу
    window.addEventListener('load', function () {
        let script = document.createElement('script');
        script.type = 'text/javascript';
        //alert(window.keyapigooglemap)
        script.src = `https://maps.googleapis.com/maps/api/js?key=AIzaSyDrd3flkuPkkAQk0zVN9r105q5_LV6SH2k&callback=initMap`;
        document.body.appendChild(script);
        
    });

    //Стилистика карты
    const style = [{
        "elementType": "geometry",
        "stylers": [{
            "color": "#e0e0e0"
        }]
    },
    {
        "elementType": "labels.icon",
        "stylers": [{
            "visibility": "off"
        }]
    },
    {
        "elementType": "labels.text.fill",
        "stylers": [{
            "color": "#616161"
        }]
    },
    {
        "elementType": "labels.text.stroke",
        "stylers": [{
            "color": "#f5f5f5"
        }]
    },
    {
        "featureType": "administrative.land_parcel",
        "elementType": "labels.text.fill",
        "stylers": [{
            "color": "#bdbdbd"
        }]
    },
    {
        "featureType": "poi",
        "elementType": "geometry",
        "stylers": [{
            "color": "#eeeeee"
        }]
    },
    {
        "featureType": "poi",
        "elementType": "labels.text.fill",
        "stylers": [{
            "color": "#757575"
        }]
    },
    {
        "featureType": "poi.park",
        "elementType": "geometry",
        "stylers": [{
            "color": "#e5e5e5"
        }]
    },
    {
        "featureType": "poi.park",
        "elementType": "labels.text.fill",
        "stylers": [{
            "color": "#9e9e9e"
        }]
    },
    {
        "featureType": "road",
        "elementType": "geometry",
        "stylers": [{
            "color": "#ffffff"
        }]
    },
    {
        "featureType": "road.arterial",
        "elementType": "labels.text.fill",
        "stylers": [{
            "color": "#757575"
        }]
    },
    {
        "featureType": "road.highway",
        "elementType": "geometry",
        "stylers": [{
            "color": "#dadada"
        }]
    },
    {
        "featureType": "road.highway",
        "elementType": "labels.text.fill",
        "stylers": [{
            "color": "#616161"
        }]
    },
    {
        "featureType": "road.local",
        "elementType": "labels.text.fill",
        "stylers": [{
            "color": "#9e9e9e"
        }]
    },
    {
        "featureType": "transit.line",
        "elementType": "geometry",
        "stylers": [{
            "color": "#e5e5e5"
        }]
    },
    {
        "featureType": "transit.station",
        "elementType": "geometry",
        "stylers": [{
            "color": "#eeeeee"
        }]
    },
    {
        "featureType": "water",
        "elementType": "geometry",
        "stylers": [{
            "color": "#c9c9c9"
        }]
    },
    {
        "featureType": "water",
        "elementType": "labels.text.fill",
        "stylers": [{
            "color": "#9e9e9e"
        }]
    }
    ];


    async function  initMap() {
        let map = new google.maps.Map(document.getElementById('js-map'), {
            center: {
                lat: 66.95,
                lng: 100.15
            },
            disableDefaultUI: true,
            zoom: 4
        });
        map.setOptions({
            styles: style
        });

        //Загружаем данные регионов
        map.data.loadGeoJson('/static/geojson/regions4.json');

        //Цветовая схема регионов РФ
        map.data.setStyle(function (feature) {
            return ({
                fillColor: '#ffffff',
                strokeColor: '#8c8c8c',
                fillOpacity: .6,
                strokeWeight: 0
            });
        });

        // Обрабатываем ховер на регионе
        map.data.addListener('mouseover', event => {
            map.data.revertStyle();
            map.data.overrideStyle(event.feature, {
                strokeWeight: 2
            });
        });

        map.data.addListener('mouseout', event => {
            map.data.revertStyle();
        });
        
        const response = await fetch('/static/geojson/data.json');
        const features = await response.json();

        if (features) {
            features.forEach(feature => {
                const marker = new google.maps.Marker({
                    position: new google.maps.LatLng(...feature.position),
                    icon: `/static/i/${feature.icon}.png`,
                    map: map
                });
                
                const infowindow = new google.maps.InfoWindow({
                    content: `<div class='custom-window'>
                        <p class='custom-window__title'>${feature.title}</p>
                        <div class='custom-window__content'>${feature.content}</div>
                    </div>`
                });
                marker.addListener('click', function() {
                    infowindow.open(map, marker);
                });
            });
        }

    }
    window.initMap = initMap;
}