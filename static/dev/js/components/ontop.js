// Кнопка наверх
$(function() {
    if ($('.js-ontop').length) {
        $(window).scroll(function() {
            if ($(this).scrollTop() > 300) {
                $('.js-ontop').addClass('ontop--show');
            } else {
                $('.js-ontop').removeClass('ontop--show');
            }
        });

        $('.js-ontop').click(function() {
            $('html, body').animate(
                {
                    scrollTop: 0
                },
                600
            );
            return false;
        });
    }
});
