import './getData';

export function checkPhone() {
    // Телефонные номера в контенте для мобильных устройств
    // и десктопов, у которых установлен и активен плагин skype click2call 

    // Утилитка для проверки зарегистрированных mime-types в браузере по подстроке параметра type.
    // Можно использовать, например, для поиска установленных плагинов в браузере, для чего и делалось.
    //
    // Проверяет параметр type во всех записях глобального массива navigator.mimeTypes
    // TODO: По идее — стоит искать в глобальном массиве navigator.plugins, но все плагины имеют свою запись в navigator.mimeTypes. Подумать на эту тему.
    //
    // Если плагин установлен и отключен, то записи в массиве не будет.
    //
    // Возвращает true, если нашлась подстрока, и false, если нет.
    // Лучше всего в качестве запроса использовать наиболее полную версию mime-type,
    // исключая изменяемые данные (например, в записи может быть номер версии — его лучше не включать в запрос)
    //
    // Для flash-плеера: checkPlugin('x-shockwave-flash'); или checkPlugin('shockwave-flash');
    // Для skype: checkPlugin('skype.click2call'); или checkPlugin('skype');
    // и т.д.
    function checkPlugin(mimetype_substr) {
        for (var i = 0; i < navigator.mimeTypes.length; i++) {
            // console.log(navigator.mimeTypes[i]['type']);
            if (navigator.mimeTypes[i]['type'].toLowerCase().indexOf(mimetype_substr) >= 0) {
                // console.log('Gotcha! Here it is: '+navigator.mimeTypes[i]['type']);
                return true;
            }
        }
        return false;
    } // <- Утилитка для проверки зарегистрированных mime-types

    // Найдем все телефоны (у них должен быть класс 'tel' и атрибут 'data-tel', в котором указан телефон без html)
    var all_phones = $('.js-tel[data-tel]');

    // Если поддерживаются touch-события делаем ссылку tel:telnum

    all_phones.each(function () {
        // Для каждого телефона проверим наличие данных в атрибуте 'data-tel'
        if ($.trim($(this).data('tel')) != '') {
            // Если данные есть — убирём возможные пробелы и пихнём их в переменную
            var tel = $(this).data('tel').replace(/\s+/ig, ''),
                // Запомним текущую разметку содержимого
                tel_content = $(this).html();
            // Пихём содержимое обратно, обернув его в правильную ссылку с соответствующим телефоном
            $(this).html('<a href="tel:' + tel + '" class="tel-link">' + tel_content + '</a>');
        }
    });

    // <- Телефонные номера в контенте для мобильных устройств
    //    и десктопов, у которых установлен и активен плагин skype click2call
}