import '../../stylus/components/styles/popupvideo.styl';
//Popup video
export function popupVideo() {
    $('body').append(`<div class="popupvideo">
                        <div class="popupvideo__wrap"><span class="popupvideo__close"></span>
                            <div class="popupvideo__video">
                                <iframe src="" frameborder="0" allowfullscreen=""></iframe>
                            </div>
                        </div>
                        <div class="popupvideo__overlay">
                        </div>
                    </div>`);
    const $popupVideo = $('.popupvideo');
    const $popapIframe = $('.popupvideo').find('iframe');
    const $popapOverlay = $('.popupvideo__overlay');
    const $popapClose = $('.popupvideo__close');
    $('.js-openvideo').on('click', function (event) {
        event.preventDefault();
        var videoSrc = $(this).attr('href');
        $popupVideo.addClass('popupvideo--show');
        $popapIframe.attr('src', videoSrc);
        return false;
    });
    $popapOverlay.on('click', function () {
        $popupVideo.removeClass('popupvideo--show');
        $popapIframe.attr('src', '');
        $('.js-player').trigger('playAllVideos');
        return false;
    });
    $popapClose.on('click', function () {
        $popupVideo.removeClass('popupvideo--show');
        $popapIframe.attr('src', '');
        $('.js-player').trigger('playAllVideos');
        return false;
    });
}