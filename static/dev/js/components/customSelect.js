import '../../stylus/components/3rdparty/chosen.styl'
import 'chosen-js';

export function customSelect(){
    $('.js-chosen').chosen({
        // отчключаем поле поиска в дропдауне при количестве пунктов меньше 10
        disable_search_threshold: 10,
        // Подхватываем кастом-классы с донорского селекта, для оформления
        inherit_select_classes: true,
        // выпиливаем выбранный вариант из общего списка
        display_selected_options: false,
        width: '100%',
        search_contains: true,
        placeholder: 'Выбрать',
        no_results_text: 'Нет совпадений',
        allow_single_deselect: false
    });
}
