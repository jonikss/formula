//import '../../stylus/components/blocks/content-slider.styl';
import '../../vendor/carouFredSel/jquery.carouFredSel-6.2.1';
import 'jquery-touchswipe/jquery.touchSwipe.min.js';

export function contentSlider() {
    const sliderConfig = {
        width: '100%',
        align: 'center',
        responsive: true,
        auto: false,
        prev: {
            button: function () {
                return $(this).parents('.content-slider').find('.content-slider__prev');
            },
            key: 'left'
        },
        next: {
            button: function () {
                return $(this).parents('.content-slider').find('.content-slider__next');
            },
            key: 'right'
        },
        pagination: {
            //container: '.content-slider__dots',
            container: function () {
                return $(this).parents('.content-slider').find('.content-slider__dots')
            },
            anchorBuilder: function (nr) {
                return '<a class="content-slider__dot"></a>';
            }
        },
        swipe: {
            onTouch: true
        },
        scroll: {
            items: 1,
            pauseOnHover: true
        },
        items: {
            filter: '.content-slider__item',
                visible: {
                    min: 1,
                    max: 3
                }
        }
    }

    $(window).on('load', function () {
        $('.content-slider').removeClass('content-slider--loading');
        $('.js-content-slider').carouFredSel(sliderConfig);
    });

    $(window).on('resize',function () {
        $('.js-content-slider').carouFredSel(sliderConfig);
    });

}