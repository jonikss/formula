import '../../stylus/components/styles/custom-window.styl';

export function contactsMap() {
    
    //Подгружаем скрипт api и присоединяем на страницу
    window.addEventListener('load', function () {
        let script = document.createElement('script');
        script.type = 'text/javascript';
        script.src = `https://maps.googleapis.com/maps/api/js?key=AIzaSyDrd3flkuPkkAQk0zVN9r105q5_LV6SH2k&callback=initMap3`;
        //document.body.appendChild(script);
        document.querySelector('head').appendChild(script);
        
    });

    function  initMap() {
        let map = new google.maps.Map(document.getElementById('js-contacts-map'), {
            center: {
                lat: 66.95,
                lng: 100.15
            },
            disableDefaultUI: true,
            zoom: 8
        });

        const marker = new google.maps.Marker({
            position: new google.maps.LatLng(66.95, 100.15),
            icon: '/static/i/marker.png',
            map: map
        });
        
        const infowindow = new google.maps.InfoWindow({
            content: `<div class='custom-window'>
                <p class='custom-window__title'>Москва</p>
                <div class='custom-window__content'>Федеральные: 258 км. <br>Территориальные: 373 км.</div>
            </div>`
        });
        marker.addListener('click', function() {
            infowindow.open(map, marker);
        });
        

    }
    window.initMap3 = initMap;
}