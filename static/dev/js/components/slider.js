//import '../../stylus/components/blocks/slider.styl';
import '../../vendor/carouFredSel/jquery.carouFredSel-6.2.1';
import 'jquery-touchswipe/jquery.touchSwipe.min.js';

export function slider() {
    var slider = $('.js-slider');
    var wrapSlider = slider.closest('.slider');
    var slider_param__auto = wrapSlider.getData('slider_auto', false),
        slider_param__auto_delay = wrapSlider.getData('slider_auto-delay', '2000'),
        slider_param__auto_timeout_duration = wrapSlider.getData('slider_auto-timeout_duration', '5000'),
        slider_param__height = wrapSlider.getData('slider_height', 'variable');

    var sliderConfig = {
        width: '100%',
        align: 'left',
        responsive: true,
        //infinity:false,
        height: slider_param__height,
        prev: {
            button: function () {
                return $(this).parents('.slider').find('.slider__prev');
            },
            key: 'left'
        },
        next: {
            button: function () {
                return $(this).parents('.slider').find('.slider__next');
            },
            key: 'right'
        },
        pagination: {
            //container: '.slider__dots',
            container: function () {
                return $(this).parents('.slider').find('.slider__dots')
            },
            anchorBuilder: function (nr) {
                return '<a class="slider__dot"></a>';
            }
        },
        swipe: {
            onTouch: true
        },
        scroll: {
            items: 1,
            pauseOnHover: true
        },
        auto: {
            play: slider_param__auto,
            timeoutDuration: slider_param__auto_delay
        },
        duration: slider_param__auto_timeout_duration,
        items: {
            visible: 1
        }
    }

    $(window).on('load', function () {
        $('.slider').removeClass('slider--loading');
        $('.js-slider').carouFredSel(sliderConfig);
    });

    $(window).on('resize', function () {
        $('.js-slider').carouFredSel(sliderConfig);
    });
}