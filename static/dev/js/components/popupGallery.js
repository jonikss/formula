import '../../stylus/components/3rdparty/photoswipe/photoswipe.css'
import '../../stylus/components/3rdparty/photoswipe/default-skin/default-skin.css'
import PhotoSwipe from 'photoswipe'
import PhotoSwipeUI_Default from 'photoswipe/dist/photoswipe-ui-default'
//window.PhotoSwipe = PhotoSwipe;
//window.PhotoSwipeUI_Default = PhotoSwipeUI_Default;

// Используем PhotoSwipe как фанси
export function popupGallery() {
    $('body').append(`<div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">
                        <div class="pswp__bg"></div>
                        <div class="pswp__scroll-wrap">
                            <div class="pswp__container">
                                <div class="pswp__item"></div>
                                <div class="pswp__item"></div>
                                <div class="pswp__item"></div>
                            </div>
                            <div class="pswp__ui pswp__ui--hidden">
                                <div class="pswp__top-bar">
                                    <div class="pswp__counter"></div>
                                    <button class="pswp__button pswp__button--close" title="Закрыть (Esc)"></button>
                                    <!--button.pswp__button.pswp__button--share(title='Share')-->
                                    <button class="pswp__button pswp__button--fs" title="На поллный экран"></button>
                                    <button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>
                                    <div class="pswp__preloader">
                                        <div class="pswp__preloader__icn">
                                            <div class="pswp__preloader__cut">
                                                <div class="pswp__preloader__donut"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
                                    <div class="pswp__share-tooltip"></div>
                                </div>
                                <button class="pswp__button pswp__button--arrow--left" title="Предыдущая (стрелка влево)"></button>
                                <button class="pswp__button pswp__button--arrow--right" title="Следующая (стрелка вправо)"></button>
                                <div class="pswp__caption">
                                    <div class="pswp__caption__center"></div>
                                </div>
                            </div>
                        </div>
                    </div>`);
    $('.js-pictures').each(function () {
        var $pic = $(this),
            getItems = function () {
                var items = [];
                $pic.find('a.js-picture').each(function () {
                    var $href = $(this).attr('href'),
                        $size = $(this).data('size').split('x'),
                        $width = $size[0],
                        $height = $size[1],
                        $caption = $(this).attr('title');
                    var item = {
                        src: $href,
                        w: $width,
                        h: $height,
                        title: $caption
                    }
                    items.push(item);
                });
                return items;
            }
        var items = getItems();

        // Preload image.
        var image = [];
        $.each(items, function (index, value) {
            image[index] = new Image();
            image[index].src = value['src'];
        });

        // Binding click event.
        var $pswp = $('.pswp')[0];
        $pic.on('click', 'a.js-picture', function (event) {
            event.preventDefault();

            var $index = $pic.find('a.js-picture').index(this);
            var options = {
                index: $index,
                bgOpacity: 0.7,
                showHideOpacity: true
            }

            var lightBox = new PhotoSwipe($pswp, PhotoSwipeUI_Default, items, options);
            lightBox.init();

            lightBox.listen('close', function () {

            });

        });
    });
}