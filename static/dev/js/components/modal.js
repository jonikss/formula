//Модалка в замен фансибокс
$(function() {
    if ($('.js-modal').length) {
        $('.js-modal').on('click', function() {
            var target = $(this).attr('href');
            $(target).addClass('modal--open');
            $('html').addClass('modal-lock');
            return false;
        });

        $('.modal__close, .js-modal-close').on('click', function(e) {
            $(this).closest('.modal').removeClass('modal--open');
            $('html').removeClass('modal-lock');
            e.stopPropagation();
            e.preventDefault();
        });

        $('.modal__overlay').on('click', function(e) {
            $(this).closest('.modal').removeClass('modal--open');
            $('html').removeClass('modal-lock');
            e.stopPropagation();
        });
        //Исключение для модалки оплаты
        //$('.modal--addtocart .modal__overlay').off('click');
    }
});
