// Получение данных из атрибутов data
// с возможностью выставить дефолтное значение ->
$.fn.getData = function(key, default_value) {
    if (this.data(key) != null && $.trim(this.data(key)) != '') {
        // Если атрибут существует и он не пустой вернём его значение
        return this.data(key);
    } else {
        if (default_value != null && default_value != '') {
            //Если в вызове было дефолтное значение есть — вернём его
            return default_value;
        } else {
            //Ничего не получилось — вернём обратно наш объект
            return false;
        }
    }
}; // <- Получение данных из атрибутов data
