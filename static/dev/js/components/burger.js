// Open meny by click burger in mobile
$(function() {
    if ($('.js-burger').length) {
        $('.js-burger').on('click', function() {
            $(this).toggleClass('burger--active');
            $('.header__mobile').toggleClass('header__mobile--show');
            return false;
        });
    }
});
