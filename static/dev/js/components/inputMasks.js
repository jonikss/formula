import 'jquery.maskedinput/src/jquery.maskedinput'

$(function() {
    if ($('.js-phone_mask').length) {
        $('.js-phone_mask').mask('+7 (999) 999-9999');
    }
    if ($('.js-date_mask').length) {
        $('.js-date_mask').mask('99.99.9999');
    }
    if ($('.js-time_mask').length) {
        $('.js-time_mask').mask('99:99');
    }
});
