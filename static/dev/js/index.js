import { isMobile } from './components/isMobile';

//Этот функционал присутсвует на всех страницах, подгружаем синхронно 
import './components/burger';
import './components/ontop';
import './components/modal';

//Пример как подгружать плагин Jquery установленный через npm
import 'jquery.maskedinput/src/jquery.maskedinput';

//Пробрасываем jQuery с некоторыми плагинами в глобальную ОВ
window.jQuery = jQuery;
window.$ = $;

//Используем async/await синтаксис
$(async () => {

    if (isMobile) {
        $('html').addClass('mobile');
    }

    try {

        //Ассинхронная подгрузка стилей
        const stylusPagesImportsFiltered = asyncStylusPages
            .filter(item => $('.' + item.className).length > 0)
            .map(item => import('../stylus/components/pages/' + item.fileName));

        const stylusBlocksImportsFiltered = asyncStylusBlocks
            .filter(item => $('.' + item.className).length > 0)
            .map(item => import('../stylus/components/blocks/' + item.fileName));

        await Promise.all([...stylusPagesImportsFiltered, stylusBlocksImportsFiltered]);

        if ($('.js-content-slider').length) {
            const { contentSlider } = await import('./components/content-slider.js');
            contentSlider();
        }

        if ($('.js-slider').length) {
            const { slider } = await import('./components/slider.js');
            slider();
        }

        if ($('.js-map').length) {
            const { map } = await import('./components/map.js');
            map();
        }

        if ($('.js-contacts-map').length) {
            const { contactsMap } = await import('./components/contacts-map.js');
            contactsMap();
        }

        if ($('.js-pictures').length) {
            const { popupGallery } = await import('./components/popupGallery.js');
            popupGallery();
        }

        if ($('.js-openvideo').length) {
            const { popupVideo } = await import('./components/popupVideo.js');
            popupVideo();
        }

        if ($('.js-chosen').length) {
            const { customSelect } = await import('./components/customSelect.js');
            customSelect();
        }

        if ($('.js-tel').length > 0 && isMobile) {
            const { checkPhone } = await import('./components/checkPhone.js');
            checkPhone();
        }

    } catch (e) {
        console.error(e);
    }


});
