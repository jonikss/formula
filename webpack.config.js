const webpack = require('webpack');
const path = require('path');
const WebpackCleanupPlugin = require("webpack-cleanup-plugin");
const fs = require('fs');

module.exports = {
    devtool: '#source-map',
    entry: './static/dev/js/index.js',
    output: {
        filename: 'bundle.js',
        chunkFilename: '[name].bundle.js',
        path: path.resolve(__dirname, 'static/js/bundles'),
        publicPath: '/static/js/bundles/'
    },
    module: {
        rules: [
            {
                test: /\.(png|jpg|gif|svg)$/,
                use: [
                    {
                        loader: 'url-loader',
                        options: {}
                    }
                ]
            },
            {
                test: /\.css$/i,
                use: [
                    'style-loader',
                    'css-loader'
                ]
            },
            {
                test: /\.styl$/i,
                use: [
                    'style-loader',
                    'css-loader',
                    {
                        loader: 'stylus-loader',
                        options: {
                            import: [path.resolve(__dirname, 'static/dev/stylus/components/common.styl')]
                        }
                    }
                ]
            },
            {
                test: /\.js$/,
                include: [
                    path.resolve(__dirname, 'static/dev/js')
                ],
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['env']
                    }
                }

            }
        ]
    },
    plugins: [
        new WebpackCleanupPlugin(),
        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery',
            "windows.jQuery": "jquery"
        }),
		//Пробрасываем список стилей которые будем прогружать ассинхронно
        new webpack.DefinePlugin({
            asyncStylusBlocks: JSON.stringify(fs.readdirSync('static/dev/stylus/components/blocks').map(file => {
                return {
                    fileName: file,
                    className: path.basename(file, '.styl')
                };
            })),
            asyncStylusPages: JSON.stringify(fs.readdirSync('static/dev/stylus/components/pages').map(file => {
                return {
                    fileName: file,
                    className: path.basename(file, '.styl')
                };
            }))
        })
    ]
}