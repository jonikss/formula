const gulp = require('gulp');
const $ = require('gulp-load-plugins')();
const pugInheritance = require('gulp-pug-inheritance');
const browserSync = require('browser-sync');
const htmlbeautify = require('gulp-html-beautify');

// paths
const src = {
    'dev': {
        'stylus': 'static/dev/stylus/',
        'pug': 'static/dev/pug/',
        'js': 'static/dev/js/',
        'vendor': 'static/dev/vendor/',
        'svg':  'static/dev/i/svg/',
        'mustaches': 'static/dev/stylus/mustaches/'
    },
    'production': {
        'css': 'static/css/',
        'html': 'layout',
        'js': 'static/js/',
        'images':  'static/i/png'
    }
};


// Сюда добавляем нужные скрипты для сборки
const vendorscripts = [
    'node_modules/jquery/dist/jquery.js',
    'node_modules/chosen-js/chosen.jquery.js',
    //'node_modules/jquery-touchswipe/jquery.touchSwipe.min.js',
    src.dev.vendor + 'carouFredSel/jquery.carouFredSel-6.2.1-packed.js',
    'node_modules/photoswipe/dist/photoswipe.min.js',
    'node_modules/photoswipe/dist/photoswipe-ui-default.min.js',
    'node_modules/jquery.maskedinput/src/jquery.maskedinput.js',
    //'node_modules/masonry-layout/dist/masonry.pkgd.js',
    'node_modules/sticky-kit/dist/sticky-kit.min.js',
    src.dev.js + '*.js'
];




// stylus compile
const  stylus = (src, dest) => () =>
    gulp.src(src)
        .pipe($.debug({title: 'Stylus:'}))
        .pipe($.plumber({
            errorHandler: $.notify.onError({
                title: 'Stylus error',
                message: '<%= error.message %>'
            })
        }))
        .pipe($.stylus({
            'include css': true,
            'url': { name: 'embedurl' },
            set: ['inline']
        }))
        .pipe($.autoprefixer('> 1%', 'last 2 versions', 'Opera 12.1'))
        .pipe($.csso())
        .pipe(gulp.dest(dest))
        .pipe($.notify({
            title: 'Stylus',
            message: 'Styl to CSS compile complete'
        }));


// Pug compile
const pug = (src, dest) => () =>
    gulp.src(src)
        .pipe($.plumber({
            errorHandler: $.notify.onError({
                title: 'Pug error',
                message: '<%= error.message %>'
            })
        }))
        .pipe($.changed(dest, {extension: '.html'}))
        .pipe($.if(global.isWatching, $.cached('pug')))
        .pipe(pugInheritance({basedir: 'static/dev/pug', skip: 'node_modules', saveInTempFile: true}))
        .pipe($.filter(['static/dev/pug/*.pug']))
        //.pipe($.filter(['*', '!*static/dev/pug/**/']))
        .pipe($.print(filepath =>
            "Jade build: " + filepath))
        .pipe($.data(file => require('./static/dev/pug/data/data.json')))
        .pipe($.pug({
            pretty: '    '
        }))
        .on('error', console.log)
        .pipe(htmlbeautify({
            indentSize: 2
        }))
        .pipe(gulp.dest(dest))
        ;


// js build
const  js = (src, dest) => () =>
    gulp.src(src)
        .pipe($.plumber({
            errorHandler: $.notify.onError({
                title: 'JS error',
                message: '<%= error.message %>'
            })
        }))
        .pipe($.concat('vendor.js'))
        .pipe($.uglify())
        .pipe(gulp.dest(dest))
        .pipe($.notify({
            title: 'JS',
            message: 'JS build complete'
        }));


// compile styl fyle for icons
const images = (src, dest) => () =>
    gulp.src(src)
        .pipe($.newer(dest))
        .pipe($.imagemin({
           svgoPlugins: [
                {removeXMLProcInst: false}, // Prevents removal of the xml header
                {removeViewBox: false}
            ]
        }))
        .pipe($.baseimg({
            styleTemplate: dest + 'images-template.styl.mustache',
            styleName: '_images-result.styl'
        }))
        .pipe($.replace("'url(", "url('"))
        .pipe($.replace(")'", "')"))
        .pipe($.debug({title: 'images:'}))
        .pipe(gulp.dest(dest))
        .on('end', function() {
            stylus();
        })
        .pipe($.notify({
            title: 'Icons',
            message: '_images_result.styl compile complete'
        }));



// tasks
// browser-sync task for starting the server.
gulp.task('browser-sync', () => {
    browserSync({
        server: {
            baseDir: "./",
            directory: true
        }
    });
});
gulp.task('stylus', stylus(src.dev.stylus + 'styles.styl', src.production.css));
gulp.task('pug', pug(src.dev.pug + '**/*.pug', src.production.html));
//gulp.task('js', js(vendorscripts, src.production.js));
gulp.task('images', images(src.dev.svg + '*.svg', src.dev.mustaches));
gulp.task('setWatch', function() {
    global.isWatching = true;
});
// Reload all Browsers
gulp.task('bs-reload',  () => browserSync.reload());

// watch
gulp.task('watch', function() {
    gulp.watch(src.dev.svg + '*.svg', ['images']);
    gulp.watch(src.dev.stylus + '**/*.styl', ['stylus']);
    gulp.watch(src.dev.pug + '**/*.pug', ['setWatch','pug']);
    //gulp.watch(src.dev.js + '*.js', ['js']);
    gulp.watch('layout/*.html', ['bs-reload']);
    gulp.watch('static/css/*.css', ['bs-reload']);
    gulp.watch('static/js/**/*.js', ['bs-reload']);
});

// default tasks
gulp.task('default', [
    //'js',
    'images',
    'pug',
    'watch',
    'browser-sync'
]);
